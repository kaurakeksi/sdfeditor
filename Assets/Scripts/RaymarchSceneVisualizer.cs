﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class RaymarchSceneVisualizer : MonoBehaviour
{
    private RenderTexture _renderTexture;
    public RenderTexture RenderTexture
    {
        get { return _renderTexture; }
    }

    private Material _combineRenderMaterial;
    public Material CombineRenderMaterial
    {
        get
        { 
            if (_combineRenderMaterial == null)
            {
                _combineRenderMaterial = RayMarcher.Instance.CombineRenderMaterial;
            }

            return _combineRenderMaterial;
        }
    }

    private Camera _camera;
    public Camera Camera
    {
        get
        {
            if (_camera == null)
            {
                _camera = GetComponent<Camera>();
            }
            return _camera;
        }
    }

    private ComputeBuffer _primitiveBuffer;
    private ComputeBuffer _aabbBuffer;
    private ComputeBuffer _sphereBuffer;
    private ComputeBuffer _boxBuffer;

    private int _previousScaleFactor;

    private void OnEnable()
    {
        CreateRenderTexture();
        _camera = GetComponent<Camera>();
    }

    private void OnDisable()
    {
        if (_renderTexture != null)
        {
            _renderTexture.Release();
        }

        if (_primitiveBuffer != null)
        {
            _primitiveBuffer.Release();
        }

        if (_aabbBuffer != null)
        {
            _aabbBuffer.Release();
        }

        if (_sphereBuffer != null)
        {
            _sphereBuffer.Release();
        }

        if (_boxBuffer != null)
        {
            _boxBuffer.Release();
        }
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dest)
    {
        // Don't run shader without anything to render
        if (RayMarcher.Instance.Primitives.Count == 0)
        {
            Debug.LogWarning("No objects to render!");
            Graphics.Blit(src, dest);
            return;
        }
        
        // Resize render texture if viewport changed
        if (_renderTexture == null)
        {
            CreateRenderTexture();
        }
        else if (_previousScaleFactor != RayMarcher.Instance.RenderTextureDownscaleFactor ||
                Camera.scaledPixelWidth != _renderTexture.width ||
                Camera.scaledPixelHeight != _renderTexture.height)
        {
            _renderTexture.Release();
            CreateRenderTexture();
        }


        var computeShader = RayMarcher.Instance.RayMarchComputeShader;

        // Keywords
        DefineKeywords(computeShader);

        // Buffers
        SetBuffers(computeShader);

        // Properties
        SetProperties(computeShader);


        computeShader.Dispatch(0, (_renderTexture.width + 8 - 1) / 8, (_renderTexture.height + 8 - 1) / 8, 1);
        Graphics.Blit(_renderTexture, dest);

        RayMarcher.Instance.CombineRenderMaterial.SetTexture("_RayMarchRender", _renderTexture);
        Graphics.Blit(src, dest, RayMarcher.Instance.CombineRenderMaterial);
    }

    private void CreateRenderTexture()
    {
        _previousScaleFactor = RayMarcher.Instance.RenderTextureDownscaleFactor;
        int scaleFactor = Mathf.Max(1, RayMarcher.Instance.RenderTextureDownscaleFactor);

        _renderTexture = new RenderTexture(Camera.scaledPixelWidth / scaleFactor, Camera.scaledPixelHeight / scaleFactor, 24, GraphicsFormat.R32G32B32A32_SFloat);
        _renderTexture.filterMode = FilterMode.Point;
        _renderTexture.enableRandomWrite = true;
        _renderTexture.Create();
    }

    private void DefineKeywords(ComputeShader computeShader)
    {
        if (RayMarcher.Instance.FastNormal)
        {
            computeShader.EnableKeyword("FAST_NORMAL");
        }
        else
        {
            computeShader.DisableKeyword("FAST_NORMAL");
        }
    }

    private void SetBuffers(ComputeShader computeShader)
    {
        if (_primitiveBuffer != null)
        {
            _primitiveBuffer.Release();
        }
        _primitiveBuffer = new ComputeBuffer(RayMarcher.Instance.Primitives.Count, 128);
        _primitiveBuffer.SetData(RayMarcher.Instance.Primitives);
        computeShader.SetBuffer(0, "_Primitives", _primitiveBuffer);

        if (_aabbBuffer != null)
        {
            _aabbBuffer.Release();
        }
        _aabbBuffer = new ComputeBuffer(RayMarcher.Instance.Aabbs.Count, 24);
        _aabbBuffer.SetData(RayMarcher.Instance.Aabbs);
        computeShader.SetBuffer(0, "_Aabbs", _aabbBuffer);

        if (_sphereBuffer != null)
        {
            _sphereBuffer.Release();
        }
        if (RayMarcher.Instance.Spheres.Count > 0)
        {
            _sphereBuffer = new ComputeBuffer(RayMarcher.Instance.Spheres.Count, 4);
            _sphereBuffer.SetData(RayMarcher.Instance.Spheres);
        }
        else    // Unity compute shader doesn't seem like empty buffers so fill in something
        {
            List<SphereData> tempSphereBuffer = new List<SphereData>();

            _sphereBuffer = new ComputeBuffer(1, 4);
            var sphere = new SphereData();
            sphere.Radius = 0.0f;
            tempSphereBuffer.Add(sphere);
            _sphereBuffer.SetData(tempSphereBuffer);
        }
        computeShader.SetBuffer(0, "_Spheres", _sphereBuffer);

        if (_boxBuffer != null)
        {
            _boxBuffer.Release();
        }
        if (RayMarcher.Instance.Boxes.Count > 0)
        {
            _boxBuffer = new ComputeBuffer(RayMarcher.Instance.Boxes.Count, 12);
            _boxBuffer.SetData(RayMarcher.Instance.Boxes);
        }
        else    // Unity compute shader doesn't seem like empty buffers so fill in something
        {
            List<BoxData> tempBoxBuffer = new List<BoxData>();

            _boxBuffer = new ComputeBuffer(1, 12);
            var box = new BoxData();
            box.Size = Vector3.zero;
            tempBoxBuffer.Add(box);
            _boxBuffer.SetData(tempBoxBuffer);
        }
        computeShader.SetBuffer(0, "_Boxes", _boxBuffer);
    }

    private void SetProperties(ComputeShader computeShader)
    {
        // Render texture
        computeShader.SetTexture(0, "_Result", _renderTexture);

        // Light
        computeShader.SetVector("_DirectionalLightDirection", RayMarcher.Instance.DirectionalLight.transform.forward);
        computeShader.SetVector("_DirectionalLightColor", RayMarcher.Instance.DirectionalLight.color);
        computeShader.SetFloat("_DirectionalLightIntensity", RayMarcher.Instance.DirectionalLight.intensity);

        // Time
        computeShader.SetFloat("_Time", Time.time);

        // Camera
        computeShader.SetVector("_CameraPosition", Camera.transform.position);
        computeShader.SetFloat("_CameraFieldOfView", Camera.fieldOfView);
        computeShader.SetMatrix("_CameraToWorldMatrix", Camera.cameraToWorldMatrix);

        // Ray marching
        computeShader.SetFloat("_MaxSteps", RayMarcher.Instance.MaxSteps);
        computeShader.SetFloat("_MinDistance", Camera.nearClipPlane);
        computeShader.SetFloat("_MaxDistance", Camera.farClipPlane);
        computeShader.SetFloat("_Epsilon", RayMarcher.Instance.Epsilon);

        // Resolution
        computeShader.SetInts("_ScreenSize", new int[] { _renderTexture.width, _renderTexture.height });
    }
}
