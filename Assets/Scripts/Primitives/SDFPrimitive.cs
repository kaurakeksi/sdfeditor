﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SDFCombineMethod
{
    Union = 0,
    Intersection = 1,
    Difference = 2
}

public enum SDFPrimitiveType
{
    Empty = 0,
    Sphere = 1,
    Box = 2,
}

[ExecuteInEditMode]
public class SDFPrimitive : MonoBehaviour
{
    [Header("Primitive")]

    [SerializeField]
    private SDFPrimitiveType _primitive;
    public SDFPrimitiveType Primitive
    {
        get { return _primitive; }
    }

    [ShowWhen("_primitive", SDFPrimitiveType.Sphere)]
    [SerializeField]
    private float _radius;
    public float Radius
    {
        get { return _radius; }
    }

    [ShowWhen("_primitive", SDFPrimitiveType.Box)]
    [SerializeField]
    private Vector3 _size;
    public Vector3 Size
    {
        get { return _size; }
    }


    [Header("Rendering")]

    [SerializeField]
    private Color _color = Color.grey;
    public Color Color
    {
        get { return _color; }
    }

    [SerializeField]
    private SDFCombineMethod _combineMethod;
    public SDFCombineMethod CombineMethod
    {
        get { return _combineMethod; }
        set { _combineMethod = value; }
    }

    [Range(0.0f,1.0f)]
    [SerializeField]
    private float _childSmoothness;
    public float ChildSmoothness
    {
        get { return _childSmoothness; }
        set { _childSmoothness = value; }
    }

    private float GetHighestParentHierarchySmoothness()
    {
        float result = 0.0f;
        var parentPrimitive = transform.parent.gameObject.GetComponentInParent<SDFPrimitive>();
        while (parentPrimitive != null)
        {
            result = Mathf.Max(result, parentPrimitive.ChildSmoothness);
            if (parentPrimitive.transform.parent != null)
            {
                parentPrimitive = parentPrimitive.transform.parent.gameObject.GetComponentInParent<SDFPrimitive>();
            }
        }

        return result;
    }

    // Returns the size of bounds without smoothness into account.
    private Vector3 GetTightPrimitiveBounds()
    {
        Vector3 result = Vector3.zero;

        Vector3 epsilon = 2.0f * Vector3.one * RayMarcher.Instance.Epsilon;
        if (_primitive == SDFPrimitiveType.Sphere)
        {
            result = 2.0f * Vector3.one * _radius + epsilon;
        }
        else if (_primitive == SDFPrimitiveType.Box)
        {
            result = _size * 2.0f + epsilon;
        }

        return result;
    }

    // Returns the size of bounds that completely contain largest possible volume of interaction for
    // currently active primitive. (i.e. Primitive bounds + highest smoothness in the above hierarchy)
    private Vector3 GetActivePrimitiveBoundsSize()
    {
        float smoothness = GetHighestParentHierarchySmoothness();
        Vector3 blendFactor = Vector3.one * 2.0f * smoothness;

        return GetTightPrimitiveBounds() + blendFactor;
    }


    // Retuns min and max vector defining primitive bounds in world space.
    public (Vector3, Vector3) GetBounds()
    {
        Vector3 extents = Vector3.Scale(GetActivePrimitiveBoundsSize() / 2.0f, transform.lossyScale);

        // Sphere/Empty shape bounds are the same regardless of rotation.
        if (_primitive == SDFPrimitiveType.Sphere || _primitive == SDFPrimitiveType.Empty)
        {
            return (transform.position - extents, transform.position + extents);
        }

        var v1 = transform.position + transform.rotation * new Vector3(extents.x, extents.y, extents.z);
        var v2 = transform.position + transform.rotation * new Vector3(extents.x, extents.y, -extents.z);
        var v3 = transform.position + transform.rotation * new Vector3(extents.x, -extents.y, extents.z);
        var v4 = transform.position + transform.rotation * new Vector3(extents.x, -extents.y, -extents.z);
        var v5 = transform.position + transform.rotation * new Vector3(-extents.x, extents.y, extents.z);
        var v6 = transform.position + transform.rotation * new Vector3(-extents.x, extents.y, -extents.z);
        var v7 = transform.position + transform.rotation * new Vector3(-extents.x, -extents.y, extents.z);
        var v8 = transform.position + transform.rotation * new Vector3(-extents.x, -extents.y, -extents.z);
        
        Vector3 min, max = Vector3.zero;
        min.x = Mathf.Min(v1.x, v2.x, v3.x, v4.x, v5.x, v6.x, v7.x, v8.x);
        min.y = Mathf.Min(v1.y, v2.y, v3.y, v4.y, v5.y, v6.y, v7.y, v8.y);
        min.z = Mathf.Min(v1.z, v2.z, v3.z, v4.z, v5.z, v6.z, v7.z, v8.z);

        max.x = Mathf.Max(v1.x, v2.x, v3.x, v4.x, v5.x, v6.x, v7.x, v8.x);
        max.y = Mathf.Max(v1.y, v2.y, v3.y, v4.y, v5.y, v6.y, v7.y, v8.y);
        max.z = Mathf.Max(v1.z, v2.z, v3.z, v4.z, v5.z, v6.z, v7.z, v8.z);

        return (min, max);
    }

    private void OnDrawGizmos()
    {
        // Set alpha channel to 0 to prevent rendering.
        // Gizmos are clickable on scene view (even when alpha is 0) so this provides scene view object selection.
        Gizmos.color = new Color(1.0f,1.0f,1.0f,0.0f);
        Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
        Gizmos.DrawCube(Vector3.zero, GetTightPrimitiveBounds());


        // Visualize bounding volume. Useful when dealing with intersections/differences
        var minmax = GetBounds();
        Vector3 size;
        size.x = Mathf.Abs(minmax.Item2.x - minmax.Item1.x);
        size.y = Mathf.Abs(minmax.Item2.y - minmax.Item1.y);
        size.z = Mathf.Abs(minmax.Item2.z - minmax.Item1.z);

        Gizmos.color = new Color(1.0f,1.0f,1.0f,0.3f);
        Gizmos.matrix = Matrix4x4.TRS(transform.position, Quaternion.identity, Vector3.one);
        Gizmos.DrawWireCube(Vector3.zero, size);
    }
}
