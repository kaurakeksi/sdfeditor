﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPrimitive
{
    Vector3 GetBoundingVolume();
}
