﻿using UnityEngine;

/// <summary>
/// Read Only attribute.
/// Attribute is use only to mark ReadOnly properties.
/// Code from https://www.patrykgalach.com/2020/01/20/readonly-attribute-in-unity-editor/.
/// </summary>
public class ReadOnlyAttribute : PropertyAttribute { }