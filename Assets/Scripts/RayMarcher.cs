using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public struct PrimitiveData
{
    public Matrix4x4 WorldToLocalMatrix;

    public Vector3 Color;
    public int SiblingIndex;

    public Vector3 Scale;
    public int HierarchyDepth;

    public Vector3 Position;
    public int PrimitiveType; // Primitive enum (0: Sphere, 1: Cube, ...)

    public int PrimitiveIndex;
    public int CombineMethod; // SDFCombineMethod enum (0: Union, 1: Intersect, 2: Substract)
    public float Smoothness;
    public int _Padding;
}

public struct AABB
{
    public Vector3 MinBounds;
    public Vector3 MaxBounds;
}

public struct SphereData
{
    public float Radius;
}

public struct BoxData
{
    public Vector3 Size;
}


[ExecuteInEditMode]
public class RayMarcher : Singleton<RayMarcher>
{
    [Header("Scene")]

    [SerializeField]
    private Light _directionalLight;
    public Light DirectionalLight
    {
        get { return _directionalLight; }
    }


    [Header("Rendering")]

    [SerializeField]
    private ComputeShader _rayMarchComputeShader;
    public ComputeShader RayMarchComputeShader
    {
        get { return _rayMarchComputeShader; }
    }

    [SerializeField]
    private Material _combineRenderMaterial;
    public Material CombineRenderMaterial
    {
        get { return _combineRenderMaterial; }
    }

    [SerializeField]
    [Range(1, 8)]
    private int _renderTextureDownscaleFactor = 2;
    public int RenderTextureDownscaleFactor
    {
        get { return _renderTextureDownscaleFactor; }
    }

    [SerializeField]
    private bool _fastNormal = true;
    public bool FastNormal
    {
        get { return _fastNormal; }
    }


    [Header("Ray marching")]

    [SerializeField]
    private int _maxSteps = 200;
    public int MaxSteps
    {
        get { return _maxSteps; }
    }

    [SerializeField]
    private float _epsilon = 0.0001f;
    public float Epsilon
    {
        get { return _epsilon; }
    }

    private List<PrimitiveData> _primitives;
    public List<PrimitiveData> Primitives
    {
        get { return _primitives; }
    }

    private List<AABB> _aabbs;
    public List<AABB> Aabbs
    {
        get { return _aabbs; }
    }

    private List<SphereData> _spheres;
    public List<SphereData> Spheres
    {
        get { return _spheres; }
    }


    private List<BoxData> _boxes;
    public List<BoxData> Boxes
    {
        get { return _boxes; }
    }

    private Camera _sceneCamera;
    public Camera SceneCamera
    {
        get
        {
            if (_sceneCamera == null)
            {
                _sceneCamera = SceneView.lastActiveSceneView.camera;
            }
            return _sceneCamera;
        }
    }
    private List<RaymarchSceneVisualizer> _visualizers;

    private Dictionary<Transform, int> _transformIndexDictionary;

    private void Start()
    {
        _primitives = new List<PrimitiveData>();
        _aabbs = new List<AABB>();
        _spheres = new List<SphereData>();
        _boxes = new List<BoxData>();

        _visualizers = new List<RaymarchSceneVisualizer>();
        _transformIndexDictionary = new Dictionary<Transform, int>();
    }

    private void OnEnable()
    {
        if (_primitives == null)
        {
            _primitives = new List<PrimitiveData>();
        }

        if (_aabbs == null)
        {
            _aabbs = new List<AABB>();
        }

        if (_spheres == null)
        {
            _spheres = new List<SphereData>();
        }

        if (_boxes == null)
        {
            _boxes = new List<BoxData>();
        }

        if (_visualizers == null)
        {
            _visualizers = new List<RaymarchSceneVisualizer>();
        }

        if (_transformIndexDictionary == null)
        {
            _transformIndexDictionary = new Dictionary<Transform, int>();
        }
    }

    private void OnDisable()
    {
        // Remove all RayMarchSceneVisualizer components from scene cameras.
        foreach (var visualizer in _visualizers)
        {
            if (visualizer != null)
            {
                DestroyImmediate(visualizer);
            }
        }
        _visualizers.Clear();
    }

    private void Update()
    {
        // Travel through hierarchy and save primitive/group information into primitives dictionary.
        UpdatePrimitives();
        
        // Add RayMarchSceneVisualizer component to any SceneCamera missing it.
        if (SceneCamera != null && SceneCamera.gameObject.GetComponent<RaymarchSceneVisualizer>() == null)
        {
            _visualizers.Add(SceneCamera.gameObject.AddComponent<RaymarchSceneVisualizer>());
        }
    }

    private void UpdatePrimitives()
    {
        _primitives.Clear();
        _aabbs.Clear();
        _spheres.Clear();
        _boxes.Clear();

        _transformIndexDictionary.Clear();
        
        CreatePrimitiveHierarchy();
    }

    // Constructs an array representation of hierarchy tree (post-order traversal).
    private void CreatePrimitiveHierarchy()
    {
        PrimitiveData root = new PrimitiveData();
        AABB aabb = new AABB();

        var primitive = GetComponent<SDFPrimitive>();
        if (primitive != null)
        {
            root.PrimitiveType = (int)primitive.Primitive;
            root.PrimitiveIndex = AddPrimitive(primitive);

            root.CombineMethod = (int)primitive.CombineMethod;
            root.Smoothness = 0.0f;

            root.HierarchyDepth = 0;

            root.Color = new Vector3(primitive.Color.r, primitive.Color.g, primitive.Color.b);

            var bounds = primitive.GetBounds();
            aabb.MinBounds = bounds.Item1;
            aabb.MaxBounds = bounds.Item2;
        }
        else
        {
            // Empty primitive
            root.PrimitiveType = -1;
            root.PrimitiveIndex = -1;
            root.SiblingIndex = -1;

            root.CombineMethod = (int)SDFCombineMethod.Union;
            root.Smoothness = 0.0f;

            root.Color = new Vector4(0.0f, 0.0f, 0.0f, 0.0f);

            aabb.MinBounds = Vector3.zero;
            aabb.MaxBounds = Vector3.zero;
        }
        root.Position = transform.position;

        // Scale must be handled separately to be exact.
        // See https://iquilezles.untergrund.net/www/articles/distfunctions/distfunctions.htm for details
        root.WorldToLocalMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one).inverse;
        root.Scale = transform.lossyScale;
        
        for (int i = transform.childCount - 1; i >= 0; --i)
        {
            // Skip disabled game objects
            var child = transform.GetChild(i);
            if (!child.gameObject.activeSelf)
            {
                continue;
            }

            AddChildren(child, 0, primitive != null ? primitive.ChildSmoothness : 0.0f);
        }

        if (_primitives.Count > 0)
        {
            _primitives.Add(root);
            _aabbs.Add(aabb);
        }
    }

    private int AddChildren(Transform currentTransform, int parentDepth, float smoothness)
    {
        var primitive = currentTransform.GetComponent<SDFPrimitive>();
        var data = new PrimitiveData();
        AABB aabb = new AABB();
        if (primitive != null)
        {
            data.Position = currentTransform.position;

            // Scale must be handled separately to be exact.
            // See https://iquilezles.untergrund.net/www/articles/distfunctions/distfunctions.htm for details
            data.WorldToLocalMatrix = Matrix4x4.TRS(currentTransform.position, currentTransform.rotation, Vector3.one).inverse;
            data.Scale = currentTransform.lossyScale;

            data.PrimitiveIndex = AddPrimitive(primitive);
            data.PrimitiveType = (int)primitive.Primitive;
            data.SiblingIndex = -1;

            data.Smoothness = smoothness;
            data.CombineMethod = (int)primitive.CombineMethod;

            data.HierarchyDepth = parentDepth + 1;

            data.Color = new Vector3(primitive.Color.r, primitive.Color.g, primitive.Color.b);

            var bounds = primitive.GetBounds();
            aabb.MinBounds = bounds.Item1;
            aabb.MaxBounds = bounds.Item2;
        }

        var childIndices = new List<int>();
        for (int i = currentTransform.childCount - 1; i >= 0; --i)
        {
            // Skip disabled game objects
            var child = currentTransform.GetChild(i);
            if (!child.gameObject.activeSelf)
            {
                continue;
            }

            int childIndex = AddChildren(child, parentDepth + 1, primitive != null ? primitive.ChildSmoothness : smoothness);
            if (childIndex != -1)
            {
                childIndices.Add(childIndex);
            }
        }

        for (int i = 1; i < childIndices.Count; ++i)
        {
            var childIndex = childIndices[i];
            var child = _primitives[childIndex];
            child.SiblingIndex = childIndices[i - 1];
            _primitives[childIndex] = child;
        }

        if (primitive != null)
        {
            _primitives.Add(data);
            _aabbs.Add(aabb);
            return _primitives.Count - 1;
        }

        return -1;
    }

    // Returns the added primitive's index on its primitive specific buffer.
    private int AddPrimitive(SDFPrimitive primitive)
    {
        if (primitive.Primitive == SDFPrimitiveType.Sphere)
        {
            var sphere = new SphereData();
            sphere.Radius = primitive.Radius;
            _spheres.Add(sphere);
            return _spheres.Count - 1;
        }
        else if (primitive.Primitive == SDFPrimitiveType.Box)
        {
            var box = new BoxData();
            box.Size = primitive.Size;
            _boxes.Add(box);
            return _boxes.Count - 1;
        }

        return -1;
    }
}
