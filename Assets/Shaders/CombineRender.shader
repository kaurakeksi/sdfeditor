﻿// Bicubic interpolation code adapted from https://github.com/libretro/common-shaders/blob/master/bicubic/shaders/bicubic-sharp.cg

Shader "Custom/CombineRender"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _RayMarchRender ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Cull Off
        ZWrite Off
        ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            float4 _MainTex_TexelSize;
            float4 _RayMarchRender_TexelSize;
            sampler2D _MainTex;
            sampler2D _RayMarchRender;
            sampler2D _CameraDepthTexture;

            float weight(float x)
            {
                float ax = abs(x);
                // Mitchel-Netravali coefficients.
                // Best psychovisual result.
                const float B = 0.1;
                const float C = 0.5;

                if (ax < 1.0)
                {
                    return
                        (
                            pow(x, 2.0) * ((12.0 - 9.0 * B - 6.0 * C) * ax + (-18.0 + 12.0 * B + 6.0 * C)) +
                            (6.0 - 2.0 * B)
                        ) / 6.0;
                }
                else if ((ax >= 1.0) && (ax < 2.0))
                {
                    return
                        (
                            pow(x, 2.0) * ((-B - 6.0 * C) * ax + (6.0 * B + 30.0 * C)) +
                            (-12.0 * B - 48.0 * C) * ax + (8.0 * B + 24.0 * C)
                        ) / 6.0;
                }
                else
                {
                    return 0.0;
                }
            }
                
            float4 weight4(float x)
            {
                return float4(
                    weight(x - 2.0),
                    weight(x - 1.0),
                    weight(x),
                    weight(x + 1.0));
            }

            float3 line_run(float ypos, float4 xpos, float4 linetaps)
            {
                return
                    tex2D(_RayMarchRender, float2(xpos.r, ypos)).rgb * linetaps.r +
                    tex2D(_RayMarchRender, float2(xpos.g, ypos)).rgb * linetaps.g +
                    tex2D(_RayMarchRender, float2(xpos.b, ypos)).rgb * linetaps.b +
                    tex2D(_RayMarchRender, float2(xpos.a, ypos)).rgb * linetaps.a;
            }

            float4 bicubic_sharp(float2 stepxy, float2 texCoord)
            {	
                float2 pos = texCoord.xy + stepxy * 0.5;
                float2 f = frac(pos / stepxy);
                    
                float4 linetaps   = weight4(1.0 - f.x);
                float4 columntaps = weight4(1.0 - f.y);

                // Make sure all taps added together is exactly 1.0, otherwise some (very small) distortion can occur
                linetaps /= linetaps.r + linetaps.g + linetaps.b + linetaps.a;
                columntaps /= columntaps.r + columntaps.g + columntaps.b + columntaps.a;

                float2 xystart = (-1.5 - f) * stepxy + pos;
                float4 xpos = float4(xystart.x, xystart.x + stepxy.x, xystart.x + stepxy.x * 2.0, xystart.x + stepxy.x * 3.0);


                // Final sum and weight normalization
                float4 final = float4(line_run(xystart.y           , xpos, linetaps) * columntaps.r +
                                line_run(xystart.y + stepxy.y      , xpos, linetaps) * columntaps.g +
                                line_run(xystart.y + stepxy.y * 2.0, xpos, linetaps) * columntaps.b +
                                line_run(xystart.y + stepxy.y * 3.0, xpos, linetaps) * columntaps.a, 1.0);
                return final;
            }

            fixed4 frag(v2f i) : SV_TARGET
            {                
                // Get depth from depth texture
                float sceneDepth = tex2D(_CameraDepthTexture, i.uv).r;

                // Linear depth between camera and far clipping plane
                sceneDepth = Linear01Depth(sceneDepth);

                // Depth as distance from camera in units
                sceneDepth = sceneDepth * _ProjectionParams.z;

                // Ray marching depth saved in alpha channel
                fixed4 rayMarchColor = tex2D(_RayMarchRender, i.uv);
                if (rayMarchColor.a < sceneDepth && rayMarchColor.a > 0)
                {
                    // Bicubic interpolation
                    return bicubic_sharp(_RayMarchRender_TexelSize.xy, i.uv);
                }

                // Scene color
                return tex2D(_MainTex, i.uv);
            }

            ENDCG
        }
    }
}
