///
/// SDFs and SDF operations based on implementations from https://iquilezles.untergrund.net/www/articles/distfunctions/distfunctions.htm
///

// SDFCombineMethod enum
#define UNION 0
#define INTERSECTION 1
#define DIFFERENCE 2

// SDF
float SphereSDF(float3 p, float radius, float3 scale)
{
    return (length(p / scale) - radius) * scale;
}

float BoxSDF(float3 p, float3 size, float3 scale)
{
    float3 q = abs(p / scale) - size;
    return (length(max(q,0.0)) + min(max(q.x,max(q.y,q.z)),0.0) * scale);
}

// Operators
float OpSmoothUnion(float d1, float d2, float k)
{
    if (k <= 0.0)
    {
        return min(d1, d2);
    }

    float h = clamp(0.5 + 0.5*(d2-d1)/k, 0.0, 1.0);
    return lerp(d2, d1, h) - k*h*(1.0-h);
}

float OpSmoothDifferece(float d1, float d2, float k)
{
    if (k <= 0.0)
    {
        return max(d1, -d2);
    }

    float h = clamp(0.5 - 0.5*(d2+d1)/k, 0.0, 1.0);
    return lerp(d1, -d2, h) + k*h*(1.0-h);
}

float OpSmoothIntersection(float d1, float d2, float k)
{
    if (k <= 0.0)
    {
        return max(d1, d2);
    }

    float h = clamp(0.5 - 0.5*(d2-d1)/k, 0.0, 1.0);
    return lerp(d2, d1, h) + k*h*(1.0-h);
}

float OpCombine(float d1, float d2, float smoothness, int combineMethod)
{
    if (combineMethod == INTERSECTION)
    {
        return OpSmoothIntersection(d1, d2, smoothness);
    }
    else if (combineMethod == DIFFERENCE)
    {
        return OpSmoothDifferece(d2, d1, smoothness);
    }

    return OpSmoothUnion(d1, d2, smoothness);
}